# Build image
FROM golang:1.18-alpine

# Software versions
ENV _GOLANGCILINT_VERSION 1.46.2

# Install tools
RUN apk --no-cache --quiet add bash curl wget git build-base

# Install golangci-lint
RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh \
  | sh -s -- -b /usr/local/bin "v${_GOLANGCILINT_VERSION}"

# Enable git store credential helper
RUN git config --global credential.helper store

# Set the working directory
WORKDIR /src

# Default command
CMD ["bash"]
